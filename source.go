package LinuxMUP

import "math/rand"
// import  "time"

// 
//                                    88                                 
//                                    88                                 
//                                    88                                 
//  ,adPPYb,d8 88       88  ,adPPYba, 88,dPPYba,   ,adPPYba,  ,adPPYba,  
// a8"    `Y88 88       88 a8P_____88 88P'    "8a a8P_____88 a8"     ""  
// 8b       88 88       88 8PP""""""" 88       d8 8PP""""""" 8b          
// "8a    ,d88 "8a,   ,a88 "8b,   ,aa 88b,   ,a8" "8b,   ,aa "8a,   ,aa  
//  `"YbbdP'88  `"YbbdP'Y8  `"Ybbd8"' 8Y"Ybbd8"'   `"Ybbd8"'  `"Ybbd8"'  
//          88                                                           
//          88                                                           
// 
//////////////////////////////////////////////////////////////////

var version = "v0.2.6"
var  keyword = [15]string{"Baptême","Batince","Bâtard","Câlice","Câlice","Ciboire","Crisse","Maudit","Ostie","Sacrament","Sacrifice","Simonaque","Tabarnak","Torrieux","Viarge"}


func Intro() string {
return "ICAgICAgICAgIC9cCiAgICAgICAgICggICkKICAgICAgLi0tLlwvLi0tLgogICAgICgvYFxfXC9fL2BcKQogICAgICcgIHtfX19ffSAgJwogICAgICAgLF8vL1xcXywKICAgICAgICAnLVwvLSc="
}


func Insult() string {
return keyword[ rand.Intn(len(keyword)) ]
}

func Version() string {
return version
}

